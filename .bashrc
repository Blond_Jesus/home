# ~/.bashrc

# If not running interactively, don't do anything
[[ $- != *i* ]] && return

# set commands
set -o vi i

# source other files
source $HOME/.config/bash/fzf_cmds
source $HOME/.config/bash/bmarks
source $HOME/.config/bash/general_fns
#source /home/dillon/.config/broot/launcher/bash/br
#source /usr/share/fzf/key-bindings.bash
#source /usr/share/fzf/completion.bash

# Aliases
alias ls='ls --color=auto'
alias nt='alacritty --working-directory $PWD & disown'
alias wttr='curl "wttr.in/colorado+springs?u&format=%c%20%t%20|%20%w%20%o"'
alias weather='curl wttr.in/colorado+springs?u'
alias radar='mpv https://radar.weather.gov/lite/N0R/PUX_loop.gif'
alias svm='ssh dillon@209.141.42.218'

alias aws='\
	ssh -i "~/.ssh/aws.pem"\
   	ubuntu@ec2-3-133-130-59.us-east-2.compute.amazonaws.com'

alias update-alacritty='\
	cat ~/.config/alacritty/template.yml\
	~/.cache/wal/colors-alacritty.yml >\
	~/.config/alacritty/alacritty.yml'

# Set environment variables
export PS1="┌─[\u \$(trimPWD)]\n└──$ "
export PAGER="less"
export EDITOR="vim"
export VISUAL="vim"
export BROWSER="qutebrowser"
export PRIMARY_MONITOR=$(xrandr -q | grep primary | awk '{print $1}')

export MYVIMRC="$HOME/.vimrc"
export MYVIMSESSIONS="$HOME/.vim/sessions"

export BOOK_DIR="$HOME/Documents/books"
export BM_DIR="$HOME/Documents/bookmarks"
export NOTE_DIR="$HOME/Documents/notes"

# PATH
export PATH="$PATH:$HOME/.scripts/"
export PATH="$PATH:$HOME/.local/bin/"

export GOPATH=$HOME/go
export PATH=$PATH:$GOPATH/bin

# Less colors for man pages
export LESS_TERMCAP_mb=$'\e[1;32m'
export LESS_TERMCAP_md=$'\e[1;32m'
export LESS_TERMCAP_me=$'\e[0m'
export LESS_TERMCAP_se=$'\e[0m'
export LESS_TERMCAP_so=$'\e[01;33m'
export LESS_TERMCAP_ue=$'\e[0m'
export LESS_TERMCAP_us=$'\e[1;4;31m'

# Import colorscheme from 'wal' asynchronously
# &   # Run the process in the background.
# ( ) # Hide shell job control messages.
(cat ~/.cache/wal/sequences &)

