#!/bin/bash

TO=/run/media/dillon/4F65-895A/games
FROM="/mnt/Games/Wii and GCN"

IFS='
'
for game in $(find "$FROM" -maxdepth 1 -regex '.*\.iso'); do
	id=$(head -c 6 $game)
	game_name=$(echo $game | awk -F'[/]' '{print $(NF)}' | awk -F'[.][i]' '{print $1}')
	read -p "copy $game_name?" copy
	if [[ $copy = "y" ]]; then
		new_dir="$TO/${game_name}[${id}]"
		mkdir $new_dir
		cp "$FROM/$game_name.iso" "$new_dir/game.iso"
		echo $game_name
	fi
done

