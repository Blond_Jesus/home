#! /bin/bash

# Config
bspc config border_width 3
bspc config window_gap   6
bspc config top_padding  16
bspc config split_ratio  0.55
bspc config automatic_scheme  alternate

bspc config borderless_monocle    true
bspc config gapless_monocle       true

bspc config focus_follows_pointer   true
bspc config pointer_follows_monitor true
bspc config pointer_action1         move
bspc config pointer_action2         resize_side

# Colors
bspc config focused_border_color '#2D9574'

# rules
bspc rule --add Pavucontrol state=floating
bspc rule --add m64py state=psuedo_tiled
bspc rule --add Zathura state=tiled

# Helper functions
start_program() # start program if it is not already running and it is installed
{
	[ ! $(pidof $1) ] && command -v $1 && $@ &
}

check_program() # check to see if program is not running and that it is installed
{
	if [ ! $(pidof $1) ] && command -v $1; then
		return 0
	fi
	return 1
}

# Start up distro agnostic programs
start_program btops
start_program compton
start_program discord
start_program redshift
start_program udiskie

check_program polybar && $HOME/.config/polybar/launch.sh &
check_program bspwm && xmodmap ~/.speedswapper

# Start up programs that need distro specific launch parameters
if [[ "$(uname --kernel-release)" == *Microsoft* ]]; then # run programs for WSL
	#start_program generate-wsl-configs &&\
	bspc config pointer_modifier mod1
	sxhkd -c $HOME/.config/wsl-configs/sxhkdrc
else
	bspc config pointer_modifier mod4
	start_program sxhkd
fi

# Monitors
#monitors=($(xrandr -q | grep \ connected | awk '{print $1}'))

#more start up stuff
feh --bg-scale "$(find $HOME/Pictures/wallpapers/ | shuf -n 1)"
wal --theme base16-spacemacs
